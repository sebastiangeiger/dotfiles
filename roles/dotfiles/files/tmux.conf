# Setting the prefix from C-b to C-a
# START:prefix
set -g prefix C-a
# END:prefix
# Free the original Ctrl-b prefix keybinding
# START:unbind
unbind C-b
# END:unbind
#setting the delay between prefix and command
# START:delay
set -s escape-time 0
# END:delay
# Ensure that we can send Ctrl-A to other apps
# START:bind_prefix
bind C-a send-prefix
# END:bind_prefix

# Set the base index for windows to 1 instead of 0
# START:index
set -g base-index 1
# END:index

# Set the base index for panes to 1 instead of 0
# START:panes_index
setw -g pane-base-index 1
# END:panes_index

# Reload the file with Prefix r
# START:reload
bind r source-file ~/.tmux.conf \; display "Reloaded!"
# END:reload

# Renaming the window
bind A command-prompt 'rename-window %%'

# splitting panes
# START:panesplit
bind | split-window -h
bind - split-window -v
# END:panesplit

# moving between panes
# START:paneselect
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
# END:paneselect

# Quick pane selection
# START:panetoggle
bind -r C-n select-window -t :-
bind -r n select-window -t :+
# END:panetoggle

# Pane resizing
# START:paneresize
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5
# END:paneresize

bind x kill-pane
bind X kill-window

# mouse support - set to on if you want to use the mouse
# START:mouse
setw -g mouse off
# END:mouse

# Set the default terminal mode to 256color mode
# START:termcolor
set -g default-terminal "screen-256color"
# END:termcolor

# enable activity alerts
#START:activity
setw -g monitor-activity on
set -g visual-activity on
#END:activity

# set the status line's colors
# START:statuscolor
set -g status-style fg=white,bg=colour234
# END:statuscolor

# set the color of the window list
# START:windowstatuscolor
setw -g window-status-style fg=colour242,bg=default,dim
# END:windowstatuscolor

# set colors for the active window
# START:activewindowstatuscolor
setw -g window-status-current-style fg=white,bg=colour52,bright
# END:activewindowstatuscolor

# pane colors
# START:panecolors
set -g pane-border-style fg=green,bg=black
set -g pane-active-border-style fg=white,bg=yellow
# END:panecolors

# Command / message line
# START:cmdlinecolors
set -g message-style fg=white,bg=black,bright
# END:cmdlinecolors

# Status line left side
# START:statusleft
set -g status-left-length 40
set -g status-left "#[fg=colour250]Session: #S #[fg=colour100]#I #[fg=colour101]#P"
# END:statusleft

# Status line right side
# 15% | 28 Nov 18:15
# START: statusright
set -g status-right "#(cat /tmp/emoji_weather_rust.stdout | tail -1) %d %b %R"
set -g status-right-style fg=colour242,bg=black
# END:statusright

# Update the status bar every sixty seconds
# START:updateinterval
set -g status-interval 60
# END:updateinterval

# Center the window list
# START:centerwindowlist
set -g status-justify centre
# END:centerwindowlist

# enable vi keys.
# START:vikeys
setw -g mode-keys vi
# END:vikeys

# better copy mode
# START:copy
unbind p
bind p paste-buffer
# See https://github.com/tmux/tmux/issues/592
bind-key -Tcopy-mode-vi 'v' send -X begin-selection
bind-key -Tcopy-mode-vi 'y' send -X copy-selection
# END:copy

# clearing the screen
bind C-k send-keys -R \; clear-history
