#!/usr/bin/ruby

require 'json'
require 'open3'

STITCH_FIX_REGEX = /^stitchfix\/([^\/]+)$/

def read_keybase_json(file_name)
  contents, error, status = Open3.capture3("keybase fs read /keybase/private/sebastiangeiger/#{file_name}")
  if status.success?
    JSON.parse(contents)
  else
    raise "Something went wrong reading '#{file_name}': #{error}"
  end
end

def write_keybase_json(file_name, contents)
  contents, error, status = Open3.capture3("keybase fs write /keybase/private/sebastiangeiger/#{file_name}", stdin_data: JSON.dump(contents))
  if status.success?
    true
  else
    raise "Something went wrong writing '#{file_name}': #{error}"
  end
end

def stitch_fix_clone(full_repo_name)
  repo_name = full_repo_name.match(STITCH_FIX_REGEX)[1]
  repos = read_keybase_json("stitchfix_repos.json")
  repos << repo_name
  write_keybase_json("stitchfix_repos.json", repos.sort.uniq)
  print_summary("stitchfix_repos.json")
end

def print_summary(file_name)
  repos = read_keybase_json(file_name)
  headline = "#{repos.count} repositories in #{file_name}:"
  puts headline
  puts "-" * headline.length
  repos.each do |repo|
    puts "  * #{repo}"
  end
end

def run_ansible
  Dir.chdir(File.join(File.dirname(File.expand_path(__FILE__)), "..", "projects", "dotfiles")) do
    Kernel.exec("bin/ansible --tags git-repositories")
  end
end

if ARGV.count == 1
  repo_name = ARGV.first
  if repo_name =~ STITCH_FIX_REGEX
    stitch_fix_clone(repo_name)
    run_ansible
  else
    $stderr.puts "Don't know how to clone '#{repo_name}' yet!"
  end
else
  $stderr.puts "usage: #{$0} repo"
  exit(false)
end
