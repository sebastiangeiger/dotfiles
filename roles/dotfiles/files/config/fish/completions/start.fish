set -l projects (ls -d ~/projects/*/ | cut -f5 -d'/' | xargs)
set -l stitchfix (ls -d ~/StitchFix/*/ | cut -f5 -d'/' | xargs)

set -l folders "$stitchfix $projects"
complete -f -c start -n "not __fish_seen_subcommand_from $folders" -a $folders
