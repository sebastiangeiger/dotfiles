set -x EDITOR "nvim"
set -x GIT_EDITOR "nvim"
set -x PATH /usr/local/bin $HOME/.gems/bin $HOME/.bin $PATH
set -x LESS "-RM~gIsw"
set -x GEM_HOME $HOME/.gems

# Abbreviations ==================
abbr --add gti "git"
abbr --add g "git"
abbr --add n "notify"
abbr --add r "rails"
abbr --add bx "bundle exec"
abbr --add gp "git push"
abbr --add gc "git ci"
abbr --add ga "git add"
abbr --add gan "git add -N ."
abbr --add gap "git ap"
abbr --add gl "git l"
abbr --add gs "git status"
abbr --add gd "git diff"
abbr --add cont "git rebase --continue"
abbr --add wip "git commit -a -m '[WIP]"
abbr --add cs "circle status"
abbr --add co "circle open"
abbr --add pu "hub browse -- pulls"
abbr --add ff "bundle exec rspec --fail-fast"
abbr --add ta "tmux attach"

# Aliases ==================
alias vim="nvim"
alias vi="nvim"
alias gro="git reset --hard origin/(git rev-parse --abbrev-ref HEAD)"
alias diff="colordiff -u"
alias git-delete-merged='git branch --merged | grep -v "\*" | xargs -n 1 git branch -d'

alias date-only="date \"+%Y-%m-%d\""

# Show full path
set -g fish_prompt_pwd_dir_length 0
set -g fish_greeting ""

source ~/.config/fish/functions/colors.fish
source ~/.config/fish/functions/secrets.fish
source ~/.asdf/asdf.fish
