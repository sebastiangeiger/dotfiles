function git-run-against-all
  set END_SHA HEAD
  set BEGINNING_SHA $argv[1]
  set COMMAND $argv[2..-1]

  set STARTING_BRANCH (git rev-parse --abbrev-ref $END_SHA)
  if test $STARTING_BRANCH = "HEAD"
    set STARTING_BRANCH (git rev-list -n 1 HEAD)
  end

  if git merge-base --is-ancestor $BEGINNING_SHA $END_SHA
    set SHA_LIST (git rev-list --ancestry-path $BEGINNING_SHA..$END_SHA)[-1..1]
    for SHA in $SHA_LIST
      echo -n "$SHA: "
      git checkout -q $SHA
      if eval $COMMAND > /dev/null 2> /dev/null
        echo "Success"
        set OUTCOMES
      else
        echo "Error"
      end
    end
    git checkout -q $STARTING_BRANCH
  else
    echo "'$BEGINNING_SHA' is not an anchestor of '$END_SHA'"
    return 1
  end
end
