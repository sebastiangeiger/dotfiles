function styling-apps
	fixops apps list --team styling --format json | jq .rows[].name | sed "s/\"\(.*\)\"/\1/"
end
