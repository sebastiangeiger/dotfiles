function git-push-all
  set TIME_BETWEEN_PUSHES 10
  set END_SHA HEAD
  set BEGINNING_SHA $argv[1]
  set REMOTE_BRANCH (git rev-parse --abbrev-ref $END_SHA)

  if git merge-base --is-ancestor $BEGINNING_SHA $END_SHA
    echo "Pushing '$BEGINNING_SHA' to '$END_SHA' on branch '$REMOTE_BRANCH'"
    set SHA_LIST (git rev-list --ancestry-path $BEGINNING_SHA..$END_SHA)
    # Go through sha list in reverse order
    for SHA in $SHA_LIST[-1..1]
      set COMMAND "git push -f origin $SHA:$REMOTE_BRANCH"
      echo $COMMAND
      if eval $COMMAND > /dev/null
        for time in (seq $TIME_BETWEEN_PUSHES)[-1..1]
          echo -en "\r$time s "
          sleep 1
        end
        echo -en "\r"
      else
        echo "$COMMAND failed"
        return 1
      end
    end
  else
    echo "'$BEGINNING_SHA' is not an anchestor of '$END_SHA'"
    return 1
  end
end
