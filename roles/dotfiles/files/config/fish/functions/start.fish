function start
  function _interpret_folder -a name
    set -l base_folders ~/StitchFix ~/projects
    for base_folder in $base_folders
      set -l potential_folder "$base_folder/$name"
      if test -d $potential_folder
        echo $potential_folder
        break
      end
    end
    functions -e _interpret_folder
  end

  set -l name $argv[1]
  set -l folder (_interpret_folder $name)
  set -l existing_sessions = (tmux ls | cut -f1 -d":")

  if test -z "$TMUX"
    # Not in a tmux session
    if contains $name $existing_sessions
      tmux attach -t $name
    else
      cd $folder && tmux new-session -s $name
    end
  else
    # In a tmux session
    if contains $name $existing_sessions
      tmux switch -t $name
    else
      cd $folder && tmux new-session -s $name -d && cd -
      tmux switch -t $name
    end
  end
end
