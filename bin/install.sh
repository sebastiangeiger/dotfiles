#!/bin/sh
echo "1. Xcode tools"
if ! command -v python &> /dev/null
then
  set -ex
  xcode-select --install
  set +ex
else
  echo "xcode tools already installed"
fi

echo "2. Checkout repository"
if [ -d ~/dotfiles ]
then
  echo "dotfiles repository already checked out"
else
  set -ex
  cd ~ && git clone https://gitlab.com/sebastiangeiger/dotfiles.git && cd ~/dotfiles && git config user.email "sebastian.geiger@gmail.com" && git config user.name "Sebastian Geiger"
  set +ex
fi

echo "3. PIP"
if test -f "~/Library/Python/2.7/bin/pip"
then
  set -ex
  curl "https://bootstrap.pypa.io/get-pip.py" -o get-pip.py && python get-pip.py
  set +ex
else
  echo "pip already installed"
fi

echo "4. Ansible"
if test -f "~/Library/Python/2.7/bin/ansible"
then
  set -ex
  ~/Library/Python/2.7/bin/pip install --user ansible
  ~/Library/Python/2.7/bin/ansible localhost -m ping
  set +ex
else
  echo "ansible already installed"
  ~/Library/Python/2.7/bin/ansible localhost -m ping
fi

echo "5. Run ansible playbook"
set -ex
cd ~/dotfiles && ~/Library/Python/2.7/bin/ansible-playbook --connection=local --inventory 127.0.0.1, -K playbook.yml
set +ex
